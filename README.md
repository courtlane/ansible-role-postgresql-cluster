postgresql-cluster
=========

Install and configure a multi node PostgreSQL cluster that is managed by patroni.<br>
Patroni creates and manages a configuration template for the cluster nodes and stores the current state in a distributed configuration store like `etcd` or `consul`.<br>
The virtual IP (VIP) is managed by `vip-manager` (Pulls cluster information from `etcd` and add virtual IP to the cluster leader).<br>

PostgreSQL will be installed from default Debian repos.
However you could change the value of `postgresql_install_from_external_repo` to true to install newer versions of PostgreSQL from external sources.  

Requirements
------------

- etcd configuration is not included in this Role and needs to be configured seperately (Role dependencies)

Role Variables
--------------

The default values for the variables are set in `defaults/main.yml`

Dependencies
------------

- etcd: Make sure that etcd is configured to accept requests via the APIv3 (Critical for newer versions of `vip-manager` and `patroni`)

Example Inventory
----------------

```yml
all:
  hosts:
  children:
    postgres_cluster:
      hosts:
        db-patroni-01:
          ansible_host: 10.199.34.121
        db-patroni-02:
          ansible_host: 10.199.34.122
        db-patroni-03:
          ansible_host: 10.199.34.123
```

Example Playbook
----------------
**There are much more options that can be passed to this role. Please take a look into `defaults/main.yml`**
```yml
# Deploy PostgresSQL cluster with 3 nodes and etcd cluster on the same nodes
- hosts: postgres_cluster
  become: yes
  gather_facts: yes
  roles:
    # Configure etcd
    - role: etcd
      etcd_version: "v3.4.23"
      etcd_download_url: https://files.repository.mueller.de/pgcluster/etcd-{{ etcd_version }}-linux-amd64.tar.gz
      etcd_protocol: http
      etcd_root_dir: "/opt/etcd"
      etcd_config:
        name: "{{ ansible_fqdn }}" # node name identifier
        data-dir: "/opt/etcd/data" # etcd data dir
        wal-dir: "/opt/etcd/wal"   # wal backup dir
        initial-cluster-state: "new" # Initialize new cluster
        listen-client-urls: "{{ etcd_protocol }}://{{ ansible_default_ipv4.address }}:{{ etcd_client_port }},{{ etcd_protocol }}://127.0.0.1:{{ etcd_client_port }}" # Listen address for client requests
        advertise-client-urls: "{{ etcd_protocol }}://{{ ansible_default_ipv4.address }}:{{ etcd_client_port }}" # URL for client requests
        listen-peer-urls: "{{ etcd_protocol }}://{{ ansible_default_ipv4.address }}:{{ etcd_peer_port }}" # Listen address for cluster peers
        initial-advertise-peer-urls: "{{ etcd_protocol }}://{{ ansible_default_ipv4.address }}:{{ etcd_peer_port }}" # # URL for cluster peers
        initial-cluster-token: "etcd-{{ patroni_cluster_name }}"  # Cluster secret token
        initial-cluster: "db-patroni-01.example.com={{ etcd_protocol }}://10.199.34.121:{{ etcd_peer_port }},db-patroni-02.example.com={{ etcd_protocol }}://10.199.34.122:{{ etcd_peer_port }},db-patroni-03.example.com={{ etcd_protocol }}://10.199.34.123:{{ etcd_peer_port }}"
        enable-grpc-gateway: true # Required for newer APIv3

    # Configure postgresql-cluster
    - role: postgresql-cluster
      # VIP-Manager
      vip_manager_package_repo: "https://files.repository.mueller.de/pgcluster/vip-manager_{{ vip_manager_version }}_Linux_x86_64.deb"
      vip_manager_version: "2.0.0"  # version to install
      vip_manager_conf: "/etc/patroni/vip-manager.yml"
      vip_manager_iface: "{{ vip_interface }}"  # interface to which the virtual ip will be added
      vip_manager_ip: "{{ cluster_vip }}"  # the virtual ip address to manage
      vip_manager_mask: "24"  # netmask for the virtual ip
      # Patroni cluster variables
      cluster_vip: "10.199.34.124"  # ip address for client access to databases in the cluster (optional)
      vip_interface: "{{ ansible_default_ipv4.interface }}"  # interface name (ex. "ens32")
      patroni_cluster_name: "postgres-cluster"  # specify the cluster name
      patroni_installation_method: "repo" # Allowed options: pip, repo
      patroni_install_version: "latest"  # or specific version (example 1.5.6). Will be ignored when patroni_installation_method is set to 'repo'
      patroni_superuser_username: "postgres"
      patroni_superuser_password: "test123!"  # please change password
      patroni_replication_username: "replicator"
      patroni_replication_password: "replica123!"  # please change password
      patroni_cluster_nodes:
        - "10.199.34.121"
        - "10.199.34.122"
        - "10.199.34.123"
      # DCS (Distributed Consensus Store)
      dcs_type: "etcd"
      patroni_etcd_hosts: # Provide etcd hosts and port
        - host: "10.199.34.121"
          port: "2379"
        - host: "10.199.34.122"
          port: "2379"
        - host: "10.199.34.123"
          port: "2379"
      # PostgreSQL variables
      postgresql_version: "14"
      postgresql_port: "5432"
      postgresql_encoding: "UTF8"  # for bootstrap only (initdb)
      postgresql_locale: "de_DE.UTF-8"  # for bootstrap only (initdb)
      postgresql_password_encryption_algorithm: "scram-sha-256"  # or "md5" if your clients do not work with passwords encrypted with SCRAM-SHA-256
      postgresql_cluster_name: "main"
      postgresql_data_dir: "/opt/postgresql/{{ postgresql_version }}/{{ postgresql_cluster_name }}"  # You can specify custom data dir path
      postgresql_wal_dir: "/opt/postgresql/pg_wal"  # custom WAL dir path (symlink will be created) [optional]
      postgresql_conf_dir: "/etc/postgresql/{{ postgresql_version }}/{{ postgresql_cluster_name }}"
      # (optional) Append your own parameters to main postgresql_parameters variable above
      append_postgresql_parameters: []
      #  - {option: "ssl", value: "on"}
      #  - {option: "ssl_cert_file", value: "{{ etcd_member_cert_dest }}"}
      #  - {option: "ssl_key_file", value: "{{ etcd_member_cert_key_dest }}"}
      # specify additional hosts that will be added to the pg_hba.conf
      append_postgresql_pg_hba:
        - {type: "host", database: "mydatabase", user: "mydb-user", address: "192.168.24.100/29", method: "{{ postgresql_password_encryption_algorithm }}"}
      # (optional) list of users to be created (if not already exists)
      postgresql_users:
        - { name: "mydb-user", password: "mydb-user", flags: "NOSUPERUSER" }
      # (optional) list of databases to be created (if not already exists)
      postgresql_databases:
        - { db: "mydatabase", owner: "mydb-user" }
```

License
-------

GPLv3

Author Information
------------------

Oleg Franko
